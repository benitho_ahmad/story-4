from django.shortcuts import render


def index(request):
    return render(request, 'main/index.html')

def home(request):
    return render(request, 'main/home.html')

def home_project(request):
    return render(request, 'main/home_project.html')